class Register::User {
    has str $.first_name;
    has str $.last_name;
    has str $.role;
    has str $.user_name;
    has str $.password;
    has str $.email;
}